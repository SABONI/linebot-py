from flask import Flask, request, abort
from linebot import LineBotApi, WebhookHandler
from linebot.exceptions import InvalidSignatureError
from linebot.models import MessageEvent, TextMessage, TextSendMessage, ImageMessage   # FileMessage, VideoMessage, AudioMessage, LocationMessage, StickerMessage

import os
import requests
from datetime import datetime, timedelta

from bs4 import BeautifulSoup
import psycopg2

# message class:TextMessage,ImageMessage,VideoMessage,AudioMessage,LocationMessage,StickerMessage,FileMessage
app = Flask(__name__)

# Channel Access Token
line_bot_api = LineBotApi('pLb/5ZVlcVczQoR8Z59Z68QJ4L/Qpjw1hWkETZLLMnwontGKxJlgYsctgY7Dyf6LXElaCSG+uHwicVBCKFe2GPktaaHwYArlv/6CoPMIMNBoimBStIxGnzwk96+eDrPzMv01/I91Yy2ekj9kNWpkDwdB04t89/1O/w1cDnyilFU=')
# Channel Secret
handler = WebhookHandler('b7be57a7f26b959db3dd9812dac76e39')


# 監聽所有來自 /callback 的 Post Request
@app.route("/callback", methods=['POST'])
def callback():
    # get X-Line-Signature header value
    signature = request.headers['X-Line-Signature']

    # get request body as text
    body = request.get_data(as_text=True)
    app.logger.info("Request body: " + body)

    # handle webhook body
    try:
        handler.handle(body, signature)
    except InvalidSignatureError:
        abort(400)
    return 'OK'


def db_insert(name, user_id, type, message_id, text, pic_url):
    userID = 'oonirtwucdnmla'
    passward = '37b73a9acbd50e58e3e5b6a0370818d86d77dfcbce9d0d8e73b0f447df6f427f'
    DATABASE_URL = "postgres://"+userID+":"+passward+"@ec2-54-83-19-244.compute-1.amazonaws.com:5432/dfdh9p6d3pn00t"
    db = psycopg2.connect(DATABASE_URL, user=userID, password=passward, sslmode='require')
    cursor = db.cursor()
    now = datetime.now()
    TWnow = now+timedelta(hours=8)
    if text:
        sqlcode = "INSERT INTO \"ananandata\" (\"name\",\"user_id\",\"type\",\"message_id\",\"date\",\"time\",\"text\",\"pic_url\") VALUES ('"+name+"','"+user_id+"','"+type+"','"+message_id+"','"+str(TWnow.strftime("%Y-%m-%d"))+"','"+str(TWnow.strftime("%H:%M:%S"))+"','"+text+"','"+pic_url+"');"

    else:
        sqlcode = "INSERT INTO \"ananandata\" (\"name\",\"user_id\",\"type\",\"message_id\",\"date\",\"time\",\"pic_url\") VALUES ('"+name+"','"+user_id+"','"+type+"','"+message_id+"','"+str(TWnow.strftime("%Y-%m-%d"))+"','"+str(TWnow.strftime("%H:%M:%S"))+"','"+pic_url+"');"
    cursor.execute(sqlcode)
    db.commit()
    db.close()


@handler.add(MessageEvent, message=ImageMessage)
def handle_content_message(event):
    try:
        profile = line_bot_api.get_profile(str(event.source).split("\"")[7])
        profile_text = profile.display_name+"\nuserID:"+profile.user_id+"\n種類:"+str(event.message.type)+"\n訊息編號:"+str(event.message.id)
        line_bot_api.push_message('Ubdbc8bd263f91b5552eddd1932e99b4c', TextSendMessage(text=str(profile_text)))
        db_insert(name=profile.display_name, user_id=profile.user_id, type=str(event.message.type), message_id=str(event.message.id), text=None, pic_url=profile.picture_url)
    except Exception as ex:
        line_bot_api.push_message('Ubdbc8bd263f91b5552eddd1932e99b4c', TextSendMessage(text=str(ex)))


'''VideoMessage
@handler.add(MessageEvent, message=VideoMessage)
def handle_content_message(event):
    try:
        profile = line_bot_api.get_profile(str(event.source).split("\"")[7])
        profile_text = profile.display_name+"\nuserID:"+profile.user_id+"\n種類:"+str(event.message.type)+"\n訊息編號:"+str(event.message.id)
        line_bot_api.push_message('Ubdbc8bd263f91b5552eddd1932e99b4c', TextSendMessage(text=str(profile_text)))
        db_insert(name=profile.display_name, user_id=profile.user_id, type=str(event.message.type),
                  message_id=str(event.message.id), text=None, pic_url=profile.picture_url)
    except Exception as ex:
        line_bot_api.push_message('Ubdbc8bd263f91b5552eddd1932e99b4c', TextSendMessage(text=str(ex)))
'''

'''LocationMessage
@handler.add(MessageEvent, message=LocationMessage)
def handle_content_message(event):
    try:
        profile = line_bot_api.get_profile(str(event.source).split("\"")[7])
        profile_text = profile.display_name+"\nuserID:"+profile.user_id+"\n種類:"+str(event.message.type)+"\n訊息編號:"+str(event.message.id)
        line_bot_api.push_message('Ubdbc8bd263f91b5552eddd1932e99b4c', TextSendMessage(text=str(profile_text)))
        db_insert(name=profile.display_name, user_id=profile.user_id, type=str(event.message.type),
                  message_id=str(event.message.id), text=None, pic_url=profile.picture_url)
    except Exception as ex:
        line_bot_api.push_message('Ubdbc8bd263f91b5552eddd1932e99b4c', TextSendMessage(text=str(ex)))
'''


'''AudioMessage
@handler.add(MessageEvent, message=AudioMessage)
def handle_content_message(event):
    try:
        profile = line_bot_api.get_profile(str(event.source).split("\"")[7])
        profile_text = profile.display_name+"\nuserID:"+profile.user_id+"\n種類:"+str(event.message.type)+"\n訊息編號:"+str(event.message.id)
        line_bot_api.push_message('Ubdbc8bd263f91b5552eddd1932e99b4c', TextSendMessage(text=str(profile_text)))
        db_insert(name=profile.display_name, user_id=profile.user_id, type=str(event.message.type),
                  message_id=str(event.message.id), text=None, pic_url=profile.picture_url)
    except Exception as ex:
        line_bot_api.push_message('Ubdbc8bd263f91b5552eddd1932e99b4c', TextSendMessage(text=str(ex)))
'''


'''StickerMessage
@handler.add(MessageEvent, message=StickerMessage)
def handle_content_message(event):
    try:
        profile = line_bot_api.get_profile(str(event.source).split("\"")[7])
        profile_text = profile.display_name+"\nuserID:"+profile.user_id+"\n種類:"+str(event.message.type)+"\n訊息編號:"+str(event.message.id)
        line_bot_api.push_message('Ubdbc8bd263f91b5552eddd1932e99b4c', TextSendMessage(text=str(profile_text)))
        db_insert(name=profile.display_name, user_id=profile.user_id, type=str(event.message.type),
                  message_id=str(event.message.id))
    except Exception as ex:
        line_bot_api.push_message('Ubdbc8bd263f91b5552eddd1932e99b4c', TextSendMessage(text=str(ex)))
'''

'''FileMessage
@handler.add(MessageEvent, message=FileMessage)
def handle_content_message(event):
    try:
        profile = line_bot_api.get_profile(str(event.source).split("\"")[7])
        profile_text = profile.display_name+"\nuserID:"+profile.user_id+"\n種類:"+str(event.message.type)+"\n訊息編號:"+str(event.message.id)
        line_bot_api.push_message('Ubdbc8bd263f91b5552eddd1932e99b4c', TextSendMessage(text=str(profile_text)))
        db_insert(name=profile.display_name, user_id=profile.user_id, type=str(event.message.type),
                  message_id=str(event.message.id), text=None, pic_url=profile.picture_url)
    except Exception as ex:
        line_bot_api.push_message('Ubdbc8bd263f91b5552eddd1932e99b4c', TextSendMessage(text=str(ex)))
'''


@handler.add(MessageEvent, message=TextMessage)
def wiki_master(event):
    try:
        profile = line_bot_api.get_profile(str(event.source).split("\"")[7])
        profile_text = profile.display_name+"\nuserID:"+profile.user_id+"\n大頭貼:\n"+profile.picture_url+"\n內容:"+str(event.message.text)
        line_bot_api.push_message('Ubdbc8bd263f91b5552eddd1932e99b4c', TextSendMessage(text=str(profile_text)))
        db_insert(name=profile.display_name, user_id=profile.user_id, type=str(event.message.type),
                  message_id=str(event.message.id), text=str(event.message.text), pic_url=profile.picture_url)
    except Exception as ex:
        line_bot_api.push_message('Ubdbc8bd263f91b5552eddd1932e99b4c', TextSendMessage(text=str(ex)))

    def is_int(s):
        try:
            int(s)
            return True
        except ValueError:
            pass

    def getsearch(text):
        try:
            messagetext = str(text).split("#")
            if (len(messagetext) == 3) & (messagetext[2] == "台灣運"):
                    return True
            else:
                return False
        except (ValueError, IndexError):
            return False

    def getSporturl(text):
        messagetext = str(text).split("#")
        league = messagetext[1].upper()
        allianceid = ""
        if league == "MLB":
            allianceid = 1
        elif league == "NPBL":
            allianceid = 2
        elif league == "CPBL":
            allianceid = 6
        elif league == "NBA":
            allianceid = 3
        elif league == "足球":
            allianceid = 4
        elif league == "日足":
            allianceid = 1
        elif league == "冰球":
            allianceid = 1
        elif league == "網球":
            allianceid = 1
        if allianceid != "":
            Sporturl = "https://www.playsport.cc/predictgame.php?allianceid=" + str(allianceid)
            return Sporturl
        else:
            return False

    def getGametype(text):
        messagetext = str(text).split("#")
        Gtype = messagetext[0].upper()
        if Gtype == "PK":
            return "td-bank-bet03"
        elif Gtype == "讓分":
            return "td-bank-bet01"
        elif Gtype == "大小":
            return "td-bank-bet02"
        else:
            return False

    def checksearch(text):
        if not getGametype(text):
            return "Wrong Game Type"
        elif not getSporturl(text):
            return "Wrong League"
        else:
            return False

    def soccerCrawler(Sporturl, Gametype):
        repo = requests.get(Sporturl)
        sp = BeautifulSoup(repo.text, 'html.parser')
        awayOdds = list()
        homeOdds = list()
        TiedGameOdds = list()
        Oddslist = list()
        if Gametype == "td-bank-bet03":
            i = 0
            for a in sp.find_all("", {"class": "td-bank-bet03"}):
                if (i % 2 == 0):
                    awayOdds.append(a.text.replace("\n", ""))
                    i = i + 1
                else:
                    homeOdds.append(a.text.replace("\n", ""))
                    i = i + 1
            for a in sp.find_all("", {"class": "td-bank-bet01"}):
                TiedGameOdds.append(a.text.replace("\n", ""))
            for i in range(0, len(TiedGameOdds) - 1):
                Oddslist.append("")
                Oddslist[i] = (awayOdds[i] + homeOdds[i] + TiedGameOdds[i])
        elif Gametype == "td-bank-bet02":
            i = 0
            for a in sp.find_all("", {"class": "td-bank-bet02"}):
                if (i % 2 == 0):
                    awayOdds.append(a.text.replace("\n", ""))
                    i = i + 1
                else:
                    homeOdds.append(a.text.replace("\n", ""))
                    i = i + 1
            for i in range(0, len(awayOdds) - 1):
                Oddslist.append("")
                Oddslist[i] = (awayOdds[i] + homeOdds[i])
        timelist = list()
        for a in range(0, len(sp.find_all("td", {"class": "td-gameinfo"}))):
            timelist.append(sp.find_all("td", {"class": "td-gameinfo"})[a].find("h4").text)
        Numlist = list()
        for a in range(0, len(sp.find_all("td", {"class": "td-gameinfo"}))):
            if (sp.find_all("td", {"class": "td-gameinfo"})[a].find("h3").text == ""):
                Numlist.append("X")
            else:
                Numlist.append(sp.find_all("td", {"class": "td-gameinfo"})[a].find("h3").text)
        teamnamelist = list()

        def has_class_and_style(tag):
            return tag.has_attr('class') and tag.has_attr('style')

        site = sp.find_all(has_class_and_style)
        for a in site:
            teamnamelist.append(a.text.replace("\n", ""))
        Gamelist = list()
        for i in range(0, len(teamnamelist) - 1):
            Gamelist.append("")
            Gamelist[i] = ("賽事編號:" + Numlist[i] + " " + timelist[i] + "\n" + teamnamelist[i] + "\n賠率:" + Oddslist[i] + "\n")
        if "".join(Oddslist) != "":
            return str("".join(Gamelist))
        else:
            return "現在沒有賽事，或沒開這種玩法"

    def crawler(Sporturl, Gametype):
        repo = requests.get(Sporturl)
        sp = BeautifulSoup(repo.text, 'html.parser')
        awayOdds = list()
        homeOdds = list()
        i = 0
        for a in sp.find_all("", {"class": str(Gametype)}):
            if (i % 2 == 0):
                awayOdds.append(a.text.replace("\n", ""))
                i = i + 1
            else:
                homeOdds.append(a.text.replace("\n", ""))
                i = i + 1
        timelist = list()
        for a in range(0, len(sp.find_all("td", {"class": "td-gameinfo"}))):
            timelist.append(sp.find_all("td", {"class": "td-gameinfo"})[a].find("h4").text)
        Numlist = list()
        for a in range(0, len(sp.find_all("td", {"class": "td-gameinfo"}))):
            if (sp.find_all("td", {"class": "td-gameinfo"})[a].find("h3").text == ""):
                Numlist.append("X")
            else:
                Numlist.append(sp.find_all("td", {"class": "td-gameinfo"})[a].find("h3").text)
        teamnamelist = list()
        awayName = list()
        homeName = list()

        def has_class_and_style(tag):
            return tag.has_attr('class') and tag.has_attr('style')
        site = sp.find_all(has_class_and_style)
        for a in site:
            teamnamelist.append(a.text.replace("\n", ""))
        for i in range(0, len(teamnamelist) - 1):
            if (i % 2 == 0):
                awayName.append(teamnamelist[i])
            else:
                homeName.append(teamnamelist[i])
        Gamelist = list()
        for i in range(0, len(homeName) - 1):
            Gamelist.append("")
            Gamelist[i] = (
                    "賽事編號:" + Numlist[i] + " " + timelist[i] + "\n" + awayName[i] + "\n@" + homeName[i] + "\n賠率:" +
                    awayOdds[i] + homeOdds[i] + "\n")
        if "".join(awayOdds) != "":
            return str("".join(Gamelist))
        else:
            return "現在沒有賽事，或沒開這種玩法"
    if getsearch(str(event.message.text)):
        if checksearch(str(event.message.text)):
            message = TextSendMessage(checksearch(str(event.message.text)))
        else:
            if (getSporturl(str(event.message.text)).split("=")[1] == "4"):
                if(getGametype(str(event.message.text)) == "td-bank-bet01"):
                    message = TextSendMessage("現在沒有賽事喔")
                else:
                    web = soccerCrawler(getSporturl(str(event.message.text)), getGametype(str(event.message.text)))
                    if(web):
                        message = TextSendMessage(web)
                    else:
                        message = TextSendMessage("現在沒有賽事喔")
            else:
                web = crawler(getSporturl(str(event.message.text)), getGametype(str(event.message.text)))
                if(web):
                    message = TextSendMessage(web)
                else:
                    message = TextSendMessage("現在沒有賽事喔")
    elif not is_int(event.message.text):
        wiki = 'https://zh.wikipedia.org/wiki/'+str(event.message.text)
        web = requests.get(wiki)
        if web:
            sp = BeautifulSoup(web.text, 'html.parser')
            site = sp.find("", {"id": "mw-content-text"}).find("p").text
            message = TextSendMessage(site)
        else:
            message = TextSendMessage("安安，這有點難，我想想")
    else:
        message = TextSendMessage(event.message.text)

    line_bot_api.reply_message(event.reply_token, message)


# run flask
if __name__ == "__main__":
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)
